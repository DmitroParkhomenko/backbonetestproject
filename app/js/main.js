require.config({
    paths: {
        'jquery': 'vendor/jquery/dist/jquery.min',
        'underscore': 'vendor/underscore/underscore-min',
        'backbone': 'vendor/backbone/backbone',
        'lodash' : 'vendor/lodash/lodash',
        'modernizr' : 'vendor/modernizr/modernizr'
    }
});

require(['modules/app'], function(AppView) {
    return AppView;
});