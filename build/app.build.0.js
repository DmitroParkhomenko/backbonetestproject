({
    appDir: "../app",
    baseUrl: "js",
    dir: "../app-build",
    optimizeCss: "standard.keepLines",
    fileExclusionRegExp: /\.git/
})