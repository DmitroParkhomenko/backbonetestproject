({
    //- paths are relative to this app.build.js file
    appDir: "../app",
    baseUrl: "js",
    //- this is the directory that the new files will be. it will be created if it doesn't exist
    dir: "../app-build",
    optimizeCss: "standard.keepLines",
    paths: {
        'jquery': 'vendor/jquery/dist/jquery.min',
        'underscore': 'vendor/underscore/underscore-min',
        'backbone': 'vendor/backbone/backbone',
        'lodash' : 'vendor/lodash/lodash',
        'modernizr' : 'vendor/modernizr/modernizr'
    },
    //optimize: "none",
    optimize: "uglify2",
    fileExclusionRegExp: /\.git/
})