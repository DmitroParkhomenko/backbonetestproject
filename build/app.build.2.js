({
    baseUrl: "../app/js",
    optimizeCss: "standard.keepLines",
    paths: {
        'jquery': 'empty:',
        'underscore': 'vendor/underscore/underscore-min',
        'backbone': 'vendor/backbone/backbone',
        'lodash' : 'vendor/lodash/lodash',
        'modernizr' : 'vendor/modernizr/modernizr'
    },
    optimize: "uglify2",
    fileExclusionRegExp: /\.git/,
    name: "main",
    out: "dist/main.js"
})