({
    mainConfigFile : "../app/js/main.js",

    baseUrl: "../app/js",
    optimizeCss: "standard.keepLines",

    optimize: "uglify2",
    fileExclusionRegExp: /\.git/,
    name: "main",
    out: "dist/main.js",
    removeCombined: true,
    preserveLicenseComments: false
})