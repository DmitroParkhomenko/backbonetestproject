({
    mainConfigFile : "../app/js/main.js",
    appDir: "../app",
    baseUrl: "js",
    dir: "../app-build",
    modules: [
        {
            name: "main"
        }
    ]
})