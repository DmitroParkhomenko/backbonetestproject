({
    baseUrl: "../app/js",
    name: "main",
    out: "dist/main.js",
    paths: {
        'jquery': 'empty:',
        'underscore': 'empty:',
        'backbone': 'empty:',
        'lodash' : 'empty:',
        'modernizr' : 'empty:'
    }
})