({
    mainConfigFile : "../app/js/main.js",
    appDir: "../app",
    baseUrl: "js",
    optimizeCss: "standard",
    fileExclusionRegExp: /\.git/,
    dir: "dist",
    modules: [
        {
            name: "main",
            exclude: [
                "infrastructure",
                "modules/app"
            ]
        }
    ]
})
