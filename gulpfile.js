var lr = require('tiny-lr'), // Минивебсервер для livereload
    gulp = require('gulp'), // Сообственно Gulp JS
    bower = require('gulp-bower'),
    jade = require('gulp-jade'), // Плагин для Jade
    stylus = require('gulp-stylus'), // Плагин для Stylus
    livereload = require('gulp-livereload'), // Livereload для Gulp
    myth = require('gulp-myth'), // Плагин для Myth - http://www.myth.io/
    csso = require('gulp-csso'), // Минификация CSS
    imagemin = require('gulp-imagemin'), // Минификация изображений
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat'), // Склейка файлов
    connect = require('connect'), // Webserver
    serveStatic = require('serve-static'),
    nib = require('nib'),
    shell = require('gulp-shell'),
    del = require('del'),
    gulpSequence = require('gulp-sequence'),
    server = lr();

var root = 'build/dist';//it var must be configured at ./config.js file!!!

gulp.task('cleanVendorDir', function (cb) {
    del([
        './build/dist/vendor'
    ], cb);
});

gulp.task('optimizeFullProject', shell.task([
    'r.js -o build/app.build.full.project.js'
]));

gulp.task('copyJqueryDependencies', function() {
    gulp.src('./app/js/vendor/jquery/**/*')
        .pipe(gulp.dest('./build/dist/js/vendor/jquery'));
});

gulp.task('buildProduction',gulpSequence('optimizeFullProject', 'copyJqueryDependencies'));

gulp.task('bowerComponents', function () {
    //bower({ directory: './bower_components/', cmd: 'update'})
    //    .pipe(concat('bowerComponents.js'))
    //    //.pipe(uglify())
    //    .pipe(gulp.dest('./app/public/js'));
    //TODO: skipped my old strange logic :)
});

// Собираем Stylus
gulp.task('stylus', function() {
    gulp.src('./' + root + '/stylus/*.styl')
        .pipe(stylus({
            use: [nib()]
        })) // собираем stylus
        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
        .pipe(myth()) // добавляем префиксы - http://www.myth.io/
        .pipe(gulp.dest('./' + root + '/public/css/')) // записываем css
        .pipe(livereload(server)); // даем команду на перезагрузку css
});

// Собираем html из Jade

gulp.task('jade', function() {
    gulp.src(['./' + root + '/template/*.jade', '!./' + root + '/template/_*.jade'])
        .pipe(jade({
            pretty: true
        }))  // Собираем Jade только в папке ./app/template/ исключая файлы с _*
        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
        .pipe(gulp.dest('./' + root + '/public/')) // Записываем собранные файлы
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
});

// Собираем JS
gulp.task('js', function() {
    gulp.src(['./' + root + '/js/**/*.js', '!./' + root + '/js/vendor/**/*.js'])
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
});

// Копируем и минимизируем изображения

gulp.task('images', function() {
    gulp.src('./' + root + '/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./' + root + '/public/img'))

});

// Локальный сервер для разработки
gulp.task('http-server', function() {
    connect()
        .use(require('connect-livereload')())
        .use(serveStatic('./' + root))
        .listen('9000');

    console.log('Server listening on http://localhost:9000');
});

//default global task
gulp.task('default', ["bowerComponents", 'build', 'watch']);

// Запуск сервера разработки gulp watch
gulp.task('watch', function() {
    // Предварительная сборка проекта
    gulp.run('stylus');
    gulp.run('jade');
    gulp.run('images');
    gulp.run('js');

    // Подключаем Livereload
    server.listen(35729, function(err) {
        if (err) return console.log(err);

        gulp.watch(root + '/stylus/**/*.styl', function() {
            gulp.run('stylus');
        });
        gulp.watch(root + '/template/**/*.jade', function() {
            gulp.run('jade');
        });
        gulp.watch(root + '/img/**/*', function() {
            gulp.run('images');
        });
        gulp.watch(root + '/js/**/*', function() {
            gulp.run('js');
        });
    });

    gulp.run('http-server');
});

gulp.task('build', function() {
    // css
    gulp.src('./' + root + '/stylus/screen.styl')
        .pipe(stylus({
            use: ['nib']
        })) // собираем stylus
        .pipe(myth()) // добавляем префиксы - http://www.myth.io/
        .pipe(csso()) // минимизируем css
        .pipe(gulp.dest('./build/css/'));// записываем css

    // jade
    gulp.src(['./' + root + '/template/*.jade', '!./' + root + '/template/_*.jade'])
        .pipe(jade())
        .pipe(gulp.dest('./build/'));

    // js
    gulp.src(['./' + root + '/js/**/*.js', '!./' + root + '/js/vendor/**/*.js'])
        .pipe(uglify())
        .pipe(gulp.dest('./build/js'));

    // image
    gulp.src('./' + root + '/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./build/img'));

    // bower components
    bower()
        .pipe(gulp.dest('./build/lib/'))
});